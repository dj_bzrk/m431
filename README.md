# Arbeitsplatz für ein Familienmitglied

<p align="center">
<img src="/Beilagen/gamingpc.jpg"  width="800" height="500">
</p>


# Inhaltsverzeichnis

* [Einführung](1.Einfuerung.md)
* [Informieren IPERKA](2.Informieren.md)
* [Planen IPERKA](3.Planen.md)
* [Entscheidung IPERKA](4.Entscheiden.md)
* [Realisieren IPERKA](5.Realisieren.md)
* [Kontrollieren IPERKA](6.Kontrollieren.md)
* [Auswerten IPERKA](7.Auswerten.md)